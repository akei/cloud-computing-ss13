package hmap;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;


public class JobFactory {

	public static Job getFileImportMapperJob(String inputpath,
			String tablename, Configuration cfg) throws IOException {
		Job job = new Job(cfg, "Imports from " + inputpath);
		job.setJarByClass(MyFileImporter.class);
		FileInputFormat.addInputPath(job, new Path(inputpath));
		job.setMapperClass(hmap.MyFileImporter.ImportMapper.class);
		job.setOutputKeyClass(ImmutableBytesWritable.class);
		//Set Format Classes 
		job.setOutputValueClass(Put.class);
		job.setOutputFormatClass(TableOutputFormat.class);
		//Set Table to write data
		job.getConfiguration().set(TableOutputFormat.OUTPUT_TABLE, tablename);
	// No Reduce Tasks are required for this kind of job
		job.setNumReduceTasks(0);
	//Recommanded by HBase @see	http://hbase.apache.org/book/mapreduce.specex.html
		job.setMapSpeculativeExecution(false);
		return job;
	
	}


}
