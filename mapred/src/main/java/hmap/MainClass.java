package hmap;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MainClass extends Configured implements Tool {
	private static final Log LOG = LogFactory.getLog(MainClass.class);

	public static void main(String[] args) {
		if (args.length < 4) {
			System.err
					.println("Usage: <import> <ZookeeperQuorumAdress> <Inputpath> <tablename>");
			ToolRunner.printGenericCommandUsage(System.err);

		} else {
			int res;
			try {
				res = ToolRunner.run(HBaseConfiguration.create(),
						new MainClass(), args);
				System.exit(res);
			} catch (Exception e) {

				e.printStackTrace();
			}

		}

	}

	public int run(String[] args) throws Exception {

		String tablename = args[3];
		String inputpath = args[2];
		String quorumaddress = args[1];
		String jobtype = args[0].toLowerCase();
		Configuration cfg = HBaseConfiguration.create();
		boolean success;

		cfg.set("hbase.zookeeper.quorum", quorumaddress);
		cfg.set("hbase.zookeeper.property.clientPort", "2181");

		LOG.info("Testing HBase Connection");

		try {
			HBaseAdmin.checkHBaseAvailable(cfg);
		} catch (ZooKeeperConnectionException e) {

			LOG.error("ZookeeperConnectionException: Zookeeper Running ??? ", e);

		} catch (IOException e) {
			LOG.error("IOException ", e);
			e.printStackTrace();
		}
		LOG.info("***********************************Connection Test Completed**********************************");
		LOG.info(jobtype);
		if (jobtype.equals("import")) {
			LOG.info("***********************************Starting Import Job**********************************");
			LOG.info("***********************************Creating Table**********************************");
			TableHelper.createTableIfNotExist(cfg, tablename, TableModel.getColumFamilies());
			Job job = JobFactory.getFileImportMapperJob(inputpath, tablename,
					cfg);
			success = job.waitForCompletion(true);
		}  

		else {
			success = false;

		}

		return success ? 0 : 1;
	}

}
