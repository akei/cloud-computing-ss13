package hmap;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
/**Utility Class to Perform common Table Operations f.e. drop and create a Table */
public  class TableHelper {
	
	//Get an Logger Instance 
	private static final Log LOG = LogFactory.getLog(TableHelper.class);
	//Creates the Table if it doesn't exist
	public static void createTableIfNotExist(Configuration cfg, String name, String[] families) {
		try {
			HBaseAdmin admin = new HBaseAdmin(cfg);
			HTableDescriptor tabledesc = new HTableDescriptor(name);
			for (String familyname : families) {
				tabledesc.addFamily(new HColumnDescriptor(familyname));
			}
			admin.createTable(tabledesc);
			admin.close();
		} catch (MasterNotRunningException e) {
			LOG.error("HBase Master Server is not Running", e);

			e.printStackTrace();
		} catch (ZooKeeperConnectionException e) {
			LOG.error("ZookeeperConnectionException: Zookeeper Running ??? ", e);
		}
		catch (IOException e) {
			LOG.error("IOException while creating table", e);

		}

	}
	//Drop and Recreate the Table
	public static void dropAndCreateTable(Configuration cfg, String name, String[] families)
	{
		try {
			HBaseAdmin admin = new HBaseAdmin(cfg);
			try {
				if(admin.tableExists(name))
				{
					admin.disableTable(name);
					admin.deleteTable(name);
					createTableIfNotExist(cfg, name, families);
					admin.close();
				}
				else{
					createTableIfNotExist(cfg, name, families);
					admin.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MasterNotRunningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ZooKeeperConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}

}
