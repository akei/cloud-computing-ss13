package hmap;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MD5Hash;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * This class Reads Data from an TSV File and Stores it an Table, Column
 * Families and Qualifiers have to be Predefinied in an TableModel Class
 * {@link TableModel}
 * 
 * 
 * 
 * */
public class MyFileImporter {

	static class ImportMapper extends
			Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

		@Override
		protected void map(LongWritable key, Text line, Context context)
				throws IOException, InterruptedException {
			Map<String, String> cfmapping = TableModel.getCFMapping();
			// Get column Qualifiers from TableModel
			String[] columqualifiers = TableModel.getColumQualifiers();
			try {

				// Split and preserve Null Tokens
				String[] values = StringUtils.splitPreserveAllTokens(
						line.toString(), '\t');
				//Create Md5 Hash as RowKey for Random distribution
				String rowkey = MD5Hash.digest(values[0] + values[1])
						.toString();

				Put put = new Put(Bytes.toBytes(rowkey));
				int i = 0;
				for (String value : values) {

					String ckey = columqualifiers[i];

					if (value == columqualifiers[i]) {
						// do nothing
					} else {
						put.add(Bytes.toBytes(cfmapping.get(ckey)),
								Bytes.toBytes(columqualifiers[i]),
								Bytes.toBytes(value));
					}

					i++;
				}
				//Store Data in the Table
				context.write(new ImmutableBytesWritable(rowkey.getBytes()),
						put);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
