package hmap;

import java.util.ArrayList;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
/**Mapping of ColumnFamily and ColumnQualifier for the OpenurlRouterlogs
 * 
 * */
public class TableModel {

	private static String[] COLUM_QUANTIFIERS = { "logDate", "logTime",
		"encryptedUserIP", "institutionResolverID",
		"routerRedirectIdentifier", "aulast", "aufirst", "auinit",
		"auinit1", "auinitm", "au", "aucorp", "atitle", "title",
		"jtitle", "stitle", "date", "ssn", "quarter", "volume", "part",
		"issue", "spage", "epage", "pages", "artnum", "issn", "eissn",
		"isbn", "coden", "sici", "genre", "btitle", "place", "pub",
		"edition", "tpages", "series", "doi", "sid" };
	
	
	private static Map<String, String> columfamilymapping= new ImmutableMap.Builder<String, String>()
			.put("logDate", "timeinfo")
			.put("logTime", "timeinfo")
			.put("encryptedUserIP", "connectioninfo")
			.put("institutionResolverID", "connectioninfo")
			.put("routerRedirectIdentifier", "connectioninfo")
			.put("aulast", "publicationinfo")
			.put("auinit", "publicationinfo")
			.put("aufirst", "publicationinfo")
			.put("auinit1", "publicationinfo")
			.put("auinitm", "publicationinfo")
			.put("au", "publicationinfo")
			.put("aucorp", "publicationinfo")
			.put("atitle", "publicationinfo")
			.put("title", "publicationinfo")
			.put("jtitle", "publicationinfo")
			.put("stitle", "publicationinfo")
			.put("date", "publicationinfo")
			.put("ssn", "publicationinfo")
			.put("quarter", "publicationinfo")
			.put("volume", "publicationinfo")
			.put("part", "publicationinfo")
			.put("issue", "publicationinfo")
			.put("spage", "publicationinfo")
			.put("epage", "publicationinfo")
			.put("pages", "publicationinfo")
			.put("artnum", "linkinfo")
			.put("issn", "linkinfo")
			.put("eissn", "linkinfo")
			.put("isbn", "linkinfo")
			.put("coden", "linkinfo")
			.put("sici", "linkinfo")
			.put("genre", "publicationinfo")
			.put("btitle", "publicationinfo")
			.put("place", "publicationinfo")
			.put("pub", "publicationinfo")
			.put("edition", "publicationinfo")
			.put("tpages", "publicationinfo")
			.put("series", "publicationinfo")
			.put("doi", "linkinfo")
			.put("sid", "linkinfo").build();
	
	public static Map<String,String> getCFMapping()
	{	

		return columfamilymapping;
	}

	public static String getMapping(String key)
	{	
	
		return columfamilymapping.get(key);
	}
	public static String[] getColumFamilies()
	{
		ArrayList<String> list = new  ArrayList<String>();
		for(String s: columfamilymapping.values())
		{
			if(!list.contains(s))
			{
				list.add(s);
			}
				
		}
			

		return list.toArray(new String[list.size()]);
	}
	public static String[] getColumQualifiers()
	{
		return COLUM_QUANTIFIERS;
		
	}
	
	
}
